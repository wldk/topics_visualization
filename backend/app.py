import logging

from flask import Flask
from flask_cors import CORS
from flask_restful import Api

from logger import logger, logger_init
from resources import AnalysisResults, UploadZip
from utils import createServiceFolders

log = logging.getLogger("werkzeug")
log.setLevel(logging.ERROR)
log.disabled = True

createServiceFolders()
logger_init()


app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"
app.config["MAX_CONTENT_LENGTH"] = 2 ** 26  # 64 MB
api = Api(app)
api.add_resource(UploadZip, "/upload")
api.add_resource(AnalysisResults, "/results/<string:uuid>")

logger.info("Application started")
app.run("127.0.0.1", 8080)
