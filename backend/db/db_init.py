from mongoengine import connect

from config.constants import MONGO_ALIAS, MONGO_DB_HOST, MONGO_DB_NAME, MONGO_DB_PORT
from logger import logger


class MongoDBConnection:
    def __enter__(self):
        self.conn = connect(
            alias=MONGO_ALIAS,
            name=MONGO_DB_NAME,
            host=MONGO_DB_HOST,
            port=MONGO_DB_PORT,
        )
        logger.debug("Connected to a database")
        return self.conn

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()
        logger.debug("Disconnected from a database")
