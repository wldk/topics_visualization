from mongoengine import DictField, Document, StringField

from config.constants import MONGO_ALIAS


class TopicNetResults(Document):
    uuid = StringField(required=True, primary_key=True)
    results = DictField(required=True)

    meta = {"db_alias": MONGO_ALIAS, "collection": "results"}
