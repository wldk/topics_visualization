from collections import namedtuple
from os import getenv

from dotenv import load_dotenv

load_dotenv()

DOWNLOADS_DIR = "downloads"
ANY_TO_TXT_DIR = "txt"
LEMMATIZED_DIR = "lemmatized"
CSV_DATA_DIR = "csv"

ANY_TO_TXT_QUERY = """any2txt"""
LEMMATIZED_QUERY_PL = """morphoDita|converter({"type":"ccl2base"})"""
LEMMATIZED_QUERY_EN = (
    """spacy({"method":"tagger","lang":"en"})|converter({"type":"ccl2base"})"""
)

SERVICE_DIRS = [DOWNLOADS_DIR, ANY_TO_TXT_DIR, LEMMATIZED_DIR, CSV_DATA_DIR]
FILE_META = namedtuple("FileMeta", "txt lemmatized csv name")

HTTP_RESPONSE_SUCCESS = 200

LANGUAGES = ["polski", "english"]
LANGUAGE_STOPWORDS = {
    "english": "./utils/stopwords-english.txt",
    "polski": "./utils/stopwords-polish.txt",
}
LANGUAGE_RESPONSES = {
    "url": {
        "polski": "Wynik analizy dostępny pod linkiem",
        "english": "Analysis results are available under the link",
    },
    "subject": {"polski": "Wyniki analizy", "english": "Analysis results"},
    "failure": {
        "polski": "Wystąpił błąd podczas analizy danych. Spróbuj ponownie później",
        "english": "An error occur while analysing the data. Please, retry later",
    },
}

MONGO_ALIAS = "results"
MONGO_DB_NAME = "topicnet"
MONGO_DB_HOST = "mongodb://mongodb"
MONGO_DB_PORT = 27017

CELERY_BROKER_URL = "redis://redis-celery:6379"
CELERY_RESULT_BACKEND = "redis://redis-celery:6379"

mail_data = namedtuple("MAIL_CREDENTIALS", "address password")
MAIL_CREDENTIALS = mail_data(getenv("MAIL_ADDRESS"), getenv("MAIL_PASSWORD"))

WEBSITE_URL = getenv("WEB_SITE_URL") or "http://localhost:3000"
