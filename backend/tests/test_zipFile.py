import os
import sys
from pathlib import Path
from zipfile import ZipFile

sys.path.insert(1, os.path.join(sys.path[0], ".."))  # noqa

from utils.zipUtils import readZippedTxtFiles


def test_readZippedTxtFiles(tmpdir):
    zipFileName = "zipFileTest.zip"
    txtFileNames = ["txtFileTest.txt", "txtFileTest2.txt"]
    textContent = "Testing the zipFile reader!?"
    for txtFile in txtFileNames:
        with open(tmpdir.join(txtFile), "w") as file:
            file.write(textContent)

    with ZipFile(tmpdir.join(zipFileName), "w") as zipFile:
        for txtFile in txtFileNames:
            zipFile.write(tmpdir.join(txtFile))

    assert len(tmpdir.listdir()) == 3

    with ZipFile(tmpdir.join(zipFileName), "r") as zipFile:
        assert len(zipFile.namelist()) == 2

    readedFiles, readedFilesContent = readZippedTxtFiles(tmpdir.join(zipFileName))

    for fileName, zippedFileName in zip(txtFileNames, readedFiles):
        assert Path(fileName).stem == Path(zippedFileName).stem

    for content in readedFilesContent:
        assert textContent.encode() == content
