import sys
from os.path import join
from pathlib import Path

sys.path.insert(1, join(sys.path[0], ".."))  # noqa

from config.constants import ANY_TO_TXT_DIR, CSV_DATA_DIR, FILE_META, LEMMATIZED_DIR
from core.lpmn_preprocessor import RequesterLPMN


def test_zipFileProcessChain():
    filePath = "./data/testData.zip"
    requester = RequesterLPMN("requester@lpmnClinetTest.com")
    chainResult = requester.runFileProcessChain(filePath, "pl")
    fileName = Path(filePath).name
    txtZipPath = Path(ANY_TO_TXT_DIR).joinpath(fileName)
    lemmatizedZipPath = Path(LEMMATIZED_DIR).joinpath(fileName)
    csvFilePath = Path(CSV_DATA_DIR).joinpath(Path(fileName).with_suffix(".csv").name)
    expectedResults = FILE_META(txtZipPath, lemmatizedZipPath, csvFilePath)
    assert chainResult == expectedResults
