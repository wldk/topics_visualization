from dataclasses import dataclass


@dataclass
class UploadRequestParams:
    file: None
    language: None
    email: None
    top_tokens: None
    topics_number: None
    iterations_number: None
