from re import sub


def vowpalize_sequence(sequence):
    counts = {}
    words = sequence.split()
    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
    vw_string = ""
    # Sorting the dictionary is optional if it would impact performance
    for key in {
        k: v for k, v in sorted(counts.items(), key=lambda item: item[1], reverse=True)
    }:
        vw_string += key + ":" + str(counts[key]) + " "

    return vw_string


def removeWhiteSpaces(string: str):
    return " ".join(string.split())


def removePunctuation(inputString: str):
    return sub(r"[^\w\s]", "", inputString)


def replaceStopWords(inputString: str, stopWords: list):
    for stopword in stopWords:
        inputString = inputString.replace(" " + stopword + " ", " ")
    return inputString
