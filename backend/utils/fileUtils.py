from os import makedirs, remove, removedirs
from pathlib import Path
from uuid import uuid4

from config.constants import DOWNLOADS_DIR, SERVICE_DIRS


def createServiceFolders():
    for _dir in SERVICE_DIRS:
        makedirs(_dir, exist_ok=True)


def cleanUpServiceFolders():
    for _dir in SERVICE_DIRS:
        removedirs(_dir)


def loadStopwords(filePath):
    stopwords = []
    with open(filePath, "r") as file:
        for line in file.readlines():
            stopwords.append(line.rstrip())
    return stopwords


def getUUID():
    return str(uuid4()).replace("-", "")


def saveFileWithUUID(file):
    uuid = getUUID()
    filePath = Path(DOWNLOADS_DIR).joinpath(uuid).with_suffix(".zip")
    file.save(filePath)
    return filePath._str


def removeFile(filePath):
    remove(filePath)
