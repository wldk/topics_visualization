import smtplib
from email.mime.text import MIMEText

from config.constants import LANGUAGE_RESPONSES, MAIL_CREDENTIALS, WEBSITE_URL
from logger import logger

SMTP_SERVER_CONFIG = {"address": "smtp.gmail.com", "port": 465}


class MailingException(Exception):
    pass


def construct_mail(subject, body, msg_type):
    mail = MIMEText(body, msg_type)
    mail["Subject"] = subject

    return mail


def send_mail(receiver_address, subject, body, msg_type="plain"):
    mail = construct_mail(subject, body, msg_type)

    with smtplib.SMTP_SSL(
        SMTP_SERVER_CONFIG["address"], SMTP_SERVER_CONFIG["port"]
    ) as server:
        server.login(MAIL_CREDENTIALS.address, MAIL_CREDENTIALS.password)
        server.sendmail(MAIL_CREDENTIALS.address, receiver_address, mail.as_string())


def sendAnalysisResult(uuid, email, lang):
    logger.warning("%s", MAIL_CREDENTIALS)
    try:
        send_mail(
            receiver_address=email,
            subject=LANGUAGE_RESPONSES["subject"][lang],
            body=f'<a href="{WEBSITE_URL}/result/{uuid}">{LANGUAGE_RESPONSES["url"][lang]}</a>',
            msg_type="html",
        )
    except Exception as e:
        raise MailingException("Sending mail failed with msg: " + str(e))


def sendFailedAnalysisResult(filename, email, lang):
    try:
        send_mail(
            receiver_address=email,
            subject=LANGUAGE_RESPONSES["subject"][lang],
            body=f"{filename} - {LANGUAGE_RESPONSES['failure'][lang]}",
        )
    except Exception as e:
        raise MailingException("Sending mail failed with msg: " + str(e))
