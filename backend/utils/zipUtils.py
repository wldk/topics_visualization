from zipfile import ZipFile


def readZippedTxtFiles(zipFilePath: str):
    """
    Reads txt files from zip archive
    and returns list of filenames without '.txt' extension
    and file data as string
    """
    filenames = []
    filesData = []
    with ZipFile(zipFilePath, "r") as zipFile:
        for file in zipFile.namelist():
            if file.endswith(".txt"):
                filenames.append(file)
                filesData.append(zipFile.read(file))
    return filenames, filesData
