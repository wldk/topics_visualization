from zipfile import is_zipfile

from email_validator import EmailNotValidError, validate_email

from config.constants import LANGUAGES


def email_type(emailStr: str):
    try:
        validEmail = validate_email(emailStr)
        return validEmail["email"]
    except EmailNotValidError as e:
        raise ValueError(e)


def zipfile_type(file):
    if is_zipfile(file.stream._file):
        file.stream.seek(0)
        return file
    raise ValueError("File is not valid ZIP archive")


def lang_type(lang_str: str):
    if lang_str in LANGUAGES:
        return lang_str
    raise ValueError("Invalid language passed.")


def top_tokens(top_t: int):
    top_t = int(top_t)
    if top_t >= 1 and top_t <= 15:
        return top_t
    raise ValueError("Invalid value passed.")


def topics_number(t_number: int):
    t_number = int(t_number)
    if t_number >= 1 and t_number <= 40:
        return t_number
    raise ValueError("Invalid value passed.")


def iterations_number(i_number: int):
    i_number = int(i_number)
    if i_number >= 1 and i_number <= 50:
        return i_number
    raise ValueError("Invalid value passed.")
