from dataclasses import asdict

from flask_restful import Resource, reqparse
from werkzeug.utils import secure_filename

from config.constants import HTTP_RESPONSE_SUCCESS
from logger import logger
from tasks.data_processor import processData
from utils import UploadRequestParams, saveFileWithUUID

from .args_types import (
    email_type,
    iterations_number,
    lang_type,
    top_tokens,
    topics_number,
    zipfile_type,
)


class UploadZip(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("file", type=zipfile_type, location="files", required=True)
    parser.add_argument("email", type=email_type, location="form", required=True)
    parser.add_argument("language", type=lang_type, location="form", required=True)
    parser.add_argument("top_tokens", type=top_tokens, location="form", required=True)
    parser.add_argument(
        "topics_number", type=topics_number, location="form", required=True
    )
    parser.add_argument(
        "iterations_number", type=iterations_number, location="form", required=True
    )

    def post(self):
        data = UploadRequestParams(**self.parser.parse_args())
        filePath = saveFileWithUUID(data.file)
        # processData(filePath, data)
        data.file = secure_filename(data.file.filename)
        processData.apply_async(
            (filePath, asdict(data)),
        )
        logger.debug("Uploaded %s", filePath)
        return {
            "message": "Uploaded {} {}".format(data.language, data.email),
        }, HTTP_RESPONSE_SUCCESS
