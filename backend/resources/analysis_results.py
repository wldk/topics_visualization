import json

from flask import Response
from flask_restful import Resource

from db import MongoDBConnection, TopicNetResults
from logger import logger


class AnalysisResults(Resource):
    def get(self, uuid):
        logger.error("REQUEST AGR: %s %s", type(uuid), uuid)
        with MongoDBConnection():
            results = TopicNetResults.objects(uuid=uuid).first().results
            return Response(
                json.dumps(results), status=200, mimetype="application/json"
            )
