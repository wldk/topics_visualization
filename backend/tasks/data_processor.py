import db
from config.constants import LANGUAGE_STOPWORDS, LANGUAGES
from core import AnalysisModel, DataPreprocessor, RequesterLPMN
from logger import logger
from utils import (
    UploadRequestParams,
    removeFile,
    sendAnalysisResult,
    sendFailedAnalysisResult,
)

from .celery_init import celery


@celery.task
def processData(raw_data_path, request_data):
    """
    Params:
        language: str
            language code of the processed files.
            Currently supports: pl - Polish, en - English
    """

    request_data = UploadRequestParams(**request_data)
    logger.debug("Starting %s", raw_data_path)
    language = request_data.language.lower()
    if language not in LANGUAGES:
        return
    try:
        fileMetaData = RequesterLPMN(request_data.email).runFileProcessChain(
            raw_data_path, request_data.language
        )
        logger.debug("Preprocessing data %s", fileMetaData)

        logger.debug("Analyzing data %s", fileMetaData)
        DataPreprocessor(LANGUAGE_STOPWORDS[language]).runInPool(fileMetaData)
        results = AnalysisModel(fileMetaData.csv).analyzeData(
            topics_number=request_data.topics_number,
            iteration_number=request_data.iterations_number,
            top_tokens_number=request_data.top_tokens,
        )

        with db.MongoDBConnection():
            db.TopicNetResults(uuid=fileMetaData.name, results=results).save()
            logger.debug("Saved to db %s", fileMetaData)

        sendAnalysisResult(
            uuid=fileMetaData.name, email=request_data.email, lang=language
        )
        logger.debug("Processing done. Results ID: %s", fileMetaData)
    except Exception as e:
        sendFailedAnalysisResult(
            filename=request_data.file, email=request_data.email, lang=language
        )
        logger.debug("Error occure %s", e)
    removeFile(raw_data_path)
