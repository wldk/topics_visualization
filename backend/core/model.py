import shutil
from pathlib import Path

import artm
from topicnet.cooking_machine import Dataset
from topicnet.cooking_machine.cubes import RegularizersModifierCube
from topicnet.cooking_machine.experiment import Experiment
from topicnet.cooking_machine.model_constructor import init_simple_default_model
from topicnet.cooking_machine.models.topic_model import TopicModel
from topicnet.viewers.top_tokens_viewer import TopTokensViewer

from logger import logger


class AnalysisModel:
    def __init__(self, data_path: str = "./csv/testData.csv"):
        """
        Params:
            data_path: str
                path to a formatted .csv dataset
        """
        self.uuid = Path(data_path).stem
        self.data = Dataset(data_path)
        self.model_artm = None
        self.topic_model = None
        self.last_model = None

    def analyzeData(
        self,
        topics_number: int = 15,
        iteration_number: int = 10,
        top_tokens_number: int = 5,
    ):
        """
        Params:
            topics_number: int
                number of topics to extract from the texts
            iteration_number: int
                number of iterations of the cube processing
            top_tokens_number: int
                number of top resulting models to take into account
        """
        logger.error("ANALYZING: %s", topics_number)

        # Clamping input parameters
        max(min(topics_number, 40), 1)
        max(min(iteration_number, 50), 1)
        max(min(top_tokens_number, 15), 1)

        self.model_artm = init_simple_default_model(
            dataset=self.data,
            modalities_to_use={"@lemmatized": 1.0},
            main_modality="@lemmatized",
            specific_topics=topics_number,
            background_topics=1,
        )

        shutil.rmtree(self.uuid, ignore_errors=True)

        self.topic_model = TopicModel(self.model_artm)
        self.topic_model.cache_theta = True

        specific_topics = [
            t for t in self.topic_model.topic_names if "background" not in t
        ]

        experiment = Experiment(
            experiment_id=self.uuid,
            save_path=self.uuid,
            topic_model=self.topic_model,
        )

        my_first_cube = RegularizersModifierCube(
            num_iter=iteration_number,
            regularizer_parameters=[
                {
                    "regularizer": artm.DecorrelatorPhiRegularizer(
                        name="decorrelation_phi",
                        class_ids="@lemmatized",
                        topic_names=specific_topics,
                    ),
                    "tau_grid": [0, 0.005, 0.01, 0.015, 0.02, 0.025],
                },
                {
                    "regularizer": artm.SmoothSparsePhiRegularizer(
                        name="smsp_phi_specific",
                        class_ids="@lemmatized",
                        topic_names=specific_topics,
                    ),
                    "tau_grid": [-0.05, 0, 0.05],
                },
                {"name": "smooth_phi_bcg", "tau_grid": [0.1]},
            ],
            verbose=True,
        )

        my_first_cube(self.topic_model, self.data)
        query = (
            "TopicKernel@lemmatized.average_contrast > 0.7 * MAXIMUM(TopicKernel@lemmatized.average_contrast) "
            "and TopicKernel@lemmatized.average_purity > 0.7 * MAXIMUM(TopicKernel@lemmatized.average_purity) "
            "and PerplexityScore@all < 1.1 * MINIMUM(PerplexityScore@all) "
            "and TopicKernel@lemmatized.average_purity -> max"
        )

        self.last_model = experiment.select(query + " COLLECT 1")[0]
        experiment.set_criteria(cube_index=1, criteria=query + " COLLECT 10")
        # experiment.show()
        outputTopics = TopTokensViewer(
            self.last_model, num_top_tokens=top_tokens_number, method="phi"
        ).view()
        return {
            "topics": self.filterTopics(outputTopics),
            "theta": self.getThetaMatrix(),
        }

    @staticmethod
    def filterTopics(topics: dict) -> dict:
        """
        Removing @lemmatized key and rounding values
        """
        outputTopics = {}
        for key, value in topics.items():
            outputTopics[key] = value["@lemmatized"]
            for _key, _value in outputTopics[key].items():
                outputTopics[key][_key] = round(float(_value), 3)
        return outputTopics

    def getThetaMatrix(self):
        """
        Returns rounded up to 3 decimals theta matrix of the experiment
        """
        if self.last_model is not None:
            matrix = self.topic_model.get_theta(dataset=self.data)
            matrix = (matrix - matrix.min()) / (matrix.max() - matrix.min())
            matrix = matrix.astype(float).round(3).T.to_dict()
            for key, value in matrix.items():
                matrix[key] = [list(value.keys()), list(value.values())]
            return matrix
