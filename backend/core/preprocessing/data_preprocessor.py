from multiprocessing.pool import ThreadPool
from os import cpu_count

from numpy import array
from pandas import DataFrame

from config.constants import FILE_META
from utils import (
    loadStopwords,
    readZippedTxtFiles,
    removePunctuation,
    removeWhiteSpaces,
    replaceStopWords,
    vowpalize_sequence,
)


class DataPreprocessor:
    def __init__(self, stopwordsFilePath: str = None):
        self.stopwords = loadStopwords(stopwordsFilePath)

    @staticmethod
    def prepareStringText(inputString: str):
        inputString = removePunctuation(inputString)
        return removeWhiteSpaces(inputString)

    def prepareData(self, row):
        """
        Assuming that texts is a list with:
            row[0] - file id
            row[1] - raw text
            row[2] - vw text
        We need to proceed raw and vw texts - row[1], row[2]
        """
        for idx, value in enumerate(row[1:], 1):
            value = self.prepareStringText(value.decode("utf-8"))
            row[idx] = value
            if idx == 2:
                """
                Replacing stopwords in lemmatized text
                """
                value = replaceStopWords(value, self.stopwords)
                value = "@lemmatized {}".format(vowpalize_sequence(value))
                row[idx] = " |".join([row[0], value])
        return row

    def loadDataForProcessing(self, paths: FILE_META) -> array:
        """
        Returns a rows from Dataframe to process in multiprocessing Pool
        """
        txtFileIds, rawFilesData = readZippedTxtFiles(paths.txt)
        _, lemFilesData = readZippedTxtFiles(paths.lemmatized)
        return DataFrame([txtFileIds, rawFilesData, lemFilesData]).T.values

    def runInPool(self, paths: FILE_META):
        """
        The name for zip files (raw data and lemmatized) is identical
        but the folder is different (txt, lemmatized)
        and the processed data will be saved in csv_data folder
        """
        rows = self.loadDataForProcessing(paths)
        output = ThreadPool(cpu_count()).map(self.prepareData, rows)
        output = array(list(output)).reshape(-1, 3)
        DataFrame(output, columns=["id", "raw_text", "vw_text"]).to_csv(
            paths.csv, index=False
        )
