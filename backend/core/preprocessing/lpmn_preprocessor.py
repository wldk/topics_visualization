from pathlib import Path

from lpmn_client.src.requester import Requester

from config.constants import (
    ANY_TO_TXT_DIR,
    ANY_TO_TXT_QUERY,
    CSV_DATA_DIR,
    FILE_META,
    LEMMATIZED_DIR,
    LEMMATIZED_QUERY_EN,
    LEMMATIZED_QUERY_PL,
)


class RequesterLPMN:
    def __init__(self, email: str, lemmatized_query: str = LEMMATIZED_QUERY_PL):
        self.requester = Requester(email)
        self.lpmn_query = lemmatized_query

    def processZip(
        self,
        zipFilePath: str,
        outputDirPath: str = ANY_TO_TXT_DIR,
        query: str = ANY_TO_TXT_QUERY,
    ) -> Path:
        """
        Params:
            zipFilePath: str
                uuid of the zip file to proceed on clarin service
            outputDirPath: str
                directory to save the file
            query: str
                lpmn query which tells clarin service how to proceed zip file
        """
        response = self.requester.upload_files([zipFilePath])[0]
        if response.status_code != 200:
            raise ValueError(
                "Request failed. Status code: {}".format(response.status_code)
            )
        response = self.requester.process_query(query, [response.content.decode()])[0]

        zipFileName = Path(zipFilePath).with_suffix(".zip").name
        filePath = Path(outputDirPath).joinpath(zipFileName)

        response, filePath = self.requester.download_response(response, filePath)
        if response.status_code == 200:
            return filePath
        raise ValueError("Request failed. Status code: {}".format(response.status_code))

    def runFileProcessChain(self, zipFilePath: str, language: str) -> FILE_META:
        """
        Params:
            zipFilePath: str
                uuid of the zip file to proceed on clarin service
            language: str
                language code of the zip contents. Currently supports: pl - Polish, en - English
        """
        anyToTxtZipPath = self.processZip(zipFilePath)
        if language == "pl":
            lemmatizedZipPath = self.processZip(
                anyToTxtZipPath, outputDirPath=LEMMATIZED_DIR, query=LEMMATIZED_QUERY_PL
            )
        else:
            lemmatizedZipPath = self.processZip(
                anyToTxtZipPath, outputDirPath=LEMMATIZED_DIR, query=LEMMATIZED_QUERY_EN
            )

        csvDataPath = Path(CSV_DATA_DIR).joinpath(
            Path(zipFilePath).with_suffix(".csv").name
        )
        fileName = Path(zipFilePath).stem
        return FILE_META(anyToTxtZipPath, lemmatizedZipPath, csvDataPath, fileName)
