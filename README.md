# Topics visualisation

Visualization of [TopicNet](https://github.com/machine-intelligence-laboratory/TopicNet) output.

## Poetry basic usage
Create virtual environment (**venv**)
- python -m venv venv

Install **poetry** in created virtual environment:
- source venv/bin/activate
- pip install poetry

Now, as the **poetry** installed, we can add dependencies to our project:
- poetry add \<package name\>

To restore all dependencies from project
(assuming that the poetry was installed [see first steps]).
If poetry is not installed, install it from **pip** (pip install poetry) and go further.
- source venv/bin/activate
- poetry install

## Tox basic usage
* tox - will perform code formatting using **black** and linting using **flake8**
* tox -e lint - will perform linting only
* tox -e format - will perform formatting only


### Using docker
Install docker and docker-compose

Locally created **.env** file should exist, as the docker-compose use it, for applying variables.
It can be created via copying existing **env.example**

Start of the containers:
```bash
souce .env
sudo docker-compose up
```

Rebuild and start of the containers after source code changes:
```bash
souce .env
sudo docker-compose up --build
```

Working services:
* frontend - localhost:3000

### Mailing usage
In order to use a email notification the **.env** file should exist in backend directory with keys:
* MAIL_ADDRESS=<value>
* MAIL_PASSWORD=<value>

### Development status
* Redis and mongo db needed for proper work. Example installation (depends on the linux distro)
```bash
apt-get install redis mongodb
```
* Check if the services are alive. Both of them should be working (acitve):
```bash
systemctl status redis
systemctl status mongodb
```
