import React from 'react';
import Button from '@material-ui/core/Button';
import { useForm, Controller } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from 'react-i18next';
import * as Yup from "yup";
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import {
    RadioGroup,
    FormControlLabel,
    Radio,
  } from "@material-ui/core";
import './styles.css'

const schema = Yup.object().shape({
    email: Yup.string()
        .email('Not an e-mail address')
        .required('e-mail required'),
    textUpload: Yup.mixed()
        .required("You need to provide a file")
        .test("type", "You need to provide a file", (value => {
            return value && value[0]
        }))
        .test("type", "We only support zips", (value) => {
            return value && value[0] && (value[0].type === "application/zip" || value[0].type === "application/x-zip-compressed");
        }),
    language: Yup.string()
        .required("You need to choose text language"),
    topics_number: Yup.mixed()
        .required("Required"),
    iterations_number: Yup.mixed()
        .required("Required"),
    top_tokens: Yup.mixed()
        .required("Required")
});

const topicsNumberMarks = [
    {
      value: 1,
      label: '1',
    },
    {
      value: 15,
      label: '15',
    },
    {
      value: 40,
      label: '40',
    }
];
  
const iterationsNumberMarks = [
    {
        value: 1,
        label: '1',
      },
      {
        value: 10,
        label: '10',
      },
      {
        value: 50,
        label: '50',
      }
]

const topTokensNumber = [
    {
        value: 1,
        label: '1',
      },
      {
        value: 5,
        label: '5',
      },
      {
        value: 15,
        label: '15',
      }
]
export default function AppForm () {
    const { t } = useTranslation();
    const { register, handleSubmit, errors, control } = useForm({
        resolver: yupResolver(schema)
    });


    const onSubmit = (data) => {
        /*
            Tutaj pozmieniaj nazwy pól na jakie masz w backendzie i sprawdź fetcha (czy trzeba jakiś header?)
        */
        const formData = new FormData();
        formData.append('language', data.language);
        formData.append('email', data.email);
        formData.append('top_tokens', data.top_tokens);
        formData.append('iterations_number', data.iterations_number);
        formData.append('topics_number', data.topics_number);
        formData.append('file', data.textUpload[0]);
        console.log(formData.values());
        fetch('http://localhost:8080/upload', {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .then(data => {
        console.log(data)
        })
        .catch(error => {
        console.error(error)
        })
    };

    return (
        <div style={{ marginLeft: "30%", marginRight: "30%", display: 'flex',
            flexDirection: "column", backgroundColor: "white", border: "1px solid blue", borderRadius: '15px', marginTop: '40px'
            }}
        >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="header">{t("File load header")}</div>
                <div className="content">
                    <input name="textUpload" id="textUpload" type="file" ref={register} label="abc"/>
                    {errors.textUpload && <p style={{ color: 'red' }}>{t(errors.textUpload.message)}</p>}
                </div>
                <div className="header">{t("Email header")}</div>
                <div className="content">
                    <TextField id="email" inputRef={register} label="email" name="email" variant="outlined"/>
                    {errors.email && <p style={{ color: 'red' }}>{t(errors.email.message)}</p>}
                </div>
                <div className="header">{t("Language header")}</div>
                <div className="content">
                    <div style={{marginLeft: "35%"}}>
                        <Controller
                            as={
                                <RadioGroup>
                                <FormControlLabel
                                    value="polski"
                                    control={<Radio color="primary" />}
                                    label="Polski"
                                />
                                <FormControlLabel
                                    value="english"
                                    control={<Radio color="primary" />}
                                    label="English"
                                />
                                </RadioGroup>
                            }
                            name="language"
                            defaultValue=""
                            control={control}
                        />
                    </div>
                    {errors.language && <p style={{ color: 'red' }}>{t(errors.language.message)}</p>}
                </div>
                <div className="header">{t("Parameters")}</div>
                <div className="content">
                    <div className="slider-container">
                        <Typography gutterBottom>
                            {t("Topics number")}
                        </Typography>
                        <Controller
                            name="topics_number"
                            control={control}
                            defaultValue={15}
                            render={(props) => (
                            <Slider
                                {...props}
                                onChange={(_, value) => {
                                props.onChange(value);
                                }}
                                valueLabelDisplay="auto"
                                max={40}
                                min={1}
                                step={1}
                                marks={topicsNumberMarks}
                            />
                            )}
                        />
                    </div>
                </div>
                <div className="content">
                    <div className="slider-container">
                        <Typography gutterBottom>
                            {t("Number of iterations")}
                        </Typography>
                        <Controller
                            name="iterations_number"
                            control={control}
                            defaultValue={10}
                            render={(props) => (
                            <Slider
                                {...props}
                                onChange={(_, value) => {
                                props.onChange(value);
                                }}
                                valueLabelDisplay="auto"
                                max={50}
                                min={1}
                                step={1}
                                marks={iterationsNumberMarks}
                            />
                            )}
                        />
                    </div>
                </div>
                <div className="content">
                    <div className="slider-container">
                        <Typography gutterBottom>
                            {t("Top tokens")}
                        </Typography>
                        <Controller
                            name="top_tokens"
                            control={control}
                            defaultValue={5}
                            render={(props) => (
                            <Slider
                                {...props}
                                onChange={(_, value) => {
                                props.onChange(value);
                                }}
                                valueLabelDisplay="auto"
                                max={15}
                                min={1}
                                step={1}
                                marks={topTokensNumber}
                            />
                            )}
                        />
                    </div>
                </div>
                <Button style={{ width: "60%", backgroundColor: "#3f51b5", color: "white", borderRadius: '15px', marginLeft: "20%", marginRight: "20%",
                    marginBottom: '50px'}} type="submit">
                {t("Submit form")}
                </Button>
            </form>
        </div>
    )
}
