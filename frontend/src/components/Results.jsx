/* eslint-disable import/no-webpack-loader-syntax */
import React, { useEffect, useState } from 'react';
import { useParams } from "react-router";
import Plot from "react-plotly.js";
import exampleDataSmall from '../exampleData/example.json';
import exampleData from '../exampleData/exampleBig.json';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import './styles.css';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: 'lightGray',
      height: '5%'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    disabledButton: {
        color: 'rgb(55,55,55)!important'
      }
  }));

export default function Results () {
    const [chartData, setChartData] = useState(undefined);
    const [chartLayout, setChartLayout] = useState(undefined);
    const [isHeatMap, setIsHeatMap] = useState(false);
    const [topics, setTopics] = useState(undefined);
    const [theta, setTheta] = useState(undefined);
    const [mode] = useState('markers+text');
    const classes = useStyles();
    let { id } = useParams();

    useEffect(() => {
        let fetchString = 'http://locahost:8080/results/' + id
        let topics
        let theta
        fetch(fetchString, {
            method: 'GET',
        })
        .then(response => response.json())
        .then(data => {
            const parsed = JSON.parse(data);
            topics = parsed["topics"];
            theta = parsed["theta"];
            setTopics(topics);
            setTheta(theta);
        })
        .catch(error => {
            console.error(error)
            topics = exampleData["topics"]
            theta = exampleData["theta"]
            setTopics(topics);
            setTheta(theta);
        })}, []);
        
    useEffect(() => {
        if (theta) {
        if (isHeatMap) {
            let topicsCount = 0;
            let textCount = 0;
            let x = [];
            let y = [];
            let thetaArr = [];
            for (let i in theta["topic_0"][0]) {
                y.push(theta["topic_0"][0][i]);
                textCount += 1;
            }
            for (let i = 0; i<textCount; i++) {
                thetaArr.push([]);
            }
            for (let i in theta) {
                for (let j in theta[i][0]) {
                    thetaArr[j].push(theta[i][1][j]);
                }
            }
            for (let i in topics) {
                topicsCount += 1;
            }
            for (let i = 1; i<= topicsCount; i++) {
                x.push('Topic' + i);
            }
            var colorscaleValue = [
                [0, '#BFDBF3'],
                [1, '#051A7B']
            ];
            const text = [];
            const words = [];
            for (let i in topics) {
                let row = '<b>';
                for (let j in topics[i]) {
                    row += j;
                    row += '</br>';
                }
                row += '</b>';
                words.push(row);
            }
            console.log(words);

            for ( var i = 0; i < y.length; i++ ) {
                let row = []
                for ( var j = 0; j < x.length; j++ ) {
                    row.push(words[j]);
                    // layout.annotations.push(result);
                }
                text.push(row)
            }
            let data = [
                {
                  z: thetaArr,
                  x: x,
                  y: y,
                  type: 'heatmap',
                  colorscale: colorscaleValue,
                  text: 'abc',
                  hovertext: text,
                }
            ];
            let layout = {
                title: '<b>Topics net visualization</b> <br><i>Theta heatmap view</i><br> ID: <b>' + id + '</b>',
                yaxis: {
                    automargin: true,
                    title: {
                        text: "Text name",
                        standoff: 20
                    },
                },
                showlegend: true,
                autosize: true,
                xaxis: {
                    automargin: true,
                    showgrid: false,
                    title: {
                        text: "Topic number",
                        standoff: 20
                    },
                }
            };
            setChartData(data);
            setChartLayout(layout);
        } else {
        let topicsCount = 0
        let tokenCount = 0

        let x = [];
        let tokensArr = [];
        let obj = {}
        let tokenNames = [];
        let tokenDesc = [];
        let tokenValues = [];
        let tokenSizes = [];
        let name = ''
        
        for (let i in topics) {
            tokenCount = 0
            obj = {}
            tokenNames = [];
            tokenValues = [];
            tokenSizes = [];
            tokenDesc = [];
            for (let j in topics[i]) {
                name = 'Name: <b>' + j + '</b><br>Value: <b>' + topics[i][j] + '</b><br>';
                tokenNames.push('<b>' + j + '</b>');
                tokenDesc.push(name);
                tokenValues.push(topics[i][j]);
                tokenSizes.push(3000*topics[i][j])
                obj["tokenNames"] = tokenNames;
                obj["tokenValues"] = tokenValues;
                obj["tokenSizes"] = tokenSizes;
                obj["tokenDesc"] = tokenDesc;
                tokenCount += 1;
            }
            tokensArr.push(Object.assign(obj))
            topicsCount += 1;
        }

        for (let i = 1; i<=tokenCount; i++ ) {
            x.push('' + i);
        }

        let colorsArr = [
            'rgba(255, 0, 0, 0.6)',
            'rgba(255, 127, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(127, 0, 0, 0.6)',
            'rgba(0, 255, 0, 0.6)',
            'rgba(0, 255, 127, 0.6)',
            'rgba(0, 255, 255, 0.6)',
            'rgba(0, 127, 255, 0.6)',
            'rgba(0, 0, 255, 0.6)',
            'rgba(127, 0, 255, 0.6)',
            'rgba(255, 0, 255, 0.6)',
            'rgba(255, 0, 127, 0.6)',
        ]

        let data = [];
        for (let i = 0; i<topicsCount; i++) {
            let trace = {
                x: x,
                y: new Array(tokenCount).fill(i+1),
                mode: mode,
                text: tokensArr[i]["tokenNames"],
                marker: {
                    color: new Array(tokenCount).fill(colorsArr[i % 11]),
                    size: tokensArr[i]["tokenSizes"]
                },
                name: 'Topic' + (i+1),
                visible: 'legendonly',
                hovertext: tokensArr[i]["tokenDesc"]
            }
            data.push(Object.assign(trace));
        }
        data[(topicsCount-1)]["name"] = "Background"

        
        let layout = {
            title: '<b>Topics net visualization</b> <br><i>Bubble chart view</i><br> ID: <b>' + id + '</b>',
            showlegend: true,
            autosize: true,
            xaxis: {
                automargin: true,
                showgrid: false,
                zeroline: false,
                title: {
                    text: "Tokens",
                    standoff: 20
                },
                tickvals: x
            },
            yaxis: {
                automargin: true,
                showgrid: false,
                zeroline: false,
                autorange: 'reversed',
                title: {
                    text: "Topic number",
                    standoff: 20
                },
                tickmode: "linear", //  If "linear", the placement of the ticks is determined by a starting position `tick0` and a tick step `dtick`
                tick0: 1,
                dtick: 1
            },
            hovermode: 'closest'
        };
        setChartData(data);
        setChartLayout(layout);
    }
        }
    }, [isHeatMap, id, topics, theta, mode]);
    

        

    return (
            <div style={{ position: 'absolute', height: '100%', width: '100%' }}>
                <div className={classes.root}>
                    <AppBar position="static" style= {{ height: '100%' }}>
                        <Toolbar style= {{ height: '100%', minHeight: '0px' }}>
                            <div style={{ width: '50%', justifyContent: 'center', textAlign: 'center', borderRight: '3px solid white'}}>
                                <Button classes={{ disabled: classes.disabledButton }}
                                        disabled={!isHeatMap} style= {{ color: 'white', width: '100%' }}
                                        onClick={() => {setIsHeatMap(false)}}>
                                    Toggle Bubble chart view
                                </Button>       
                            </div>
                            <div style={{ width: '50%', justifyContent: 'center', textAlign: 'center'}}>
                                <Button classes={{ disabled: classes.disabledButton }}
                                        disabled={isHeatMap} style= {{ color: 'white', width: '100%' }} 
                                        onClick={() => {setIsHeatMap(true)}}>
                                    Toggle Heatmap view
                                </Button>
                            </div>
                        </Toolbar>
                    </AppBar>
                </div>
                {isHeatMap &&
                    <Plot 
                        data={chartData}
                        layout={chartLayout}
                        style={{ width: '100%', height: '95%' }}
                    />
                }
                {!isHeatMap && 
                    <Plot
                        data={chartData}
                        layout={chartLayout}
                        style={{ width: '100%', height: '95%' }}
                    />
                }
            </div>
    )
}
