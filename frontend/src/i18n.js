import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: {
      "File load header": "Choose file to load:",
      "Email header": "Enter you e-mail address:",
      "We only support zips": "We only support zips.",
      "You need to provide a file": "You need to provide a file",
      "e-mail required": "Please, enter you e-mail address.",
      "Not an e-mail address": "Not an e-mail address",
      "Submit form": "Submit",
      "Language header": "Text language",
      "You need to choose text language": "You need to choose text language",
      "Parameters": "Parameters",
      "Topics number": "Topics number",
      "Number of iterations": "Number of iterations",
      "Top tokens": "Top tokens"
    }
  },
  pl: {
    translation: {
      "File load header": "Wybierz plik do wczytania:",
      "Email header": "Podaj Twój adres e-mail:",
      "We only support zips": "Tylko pliki ZIP są obsługiwane.",
      "You need to provide a file": "Musisz wczytać plik.",
      "e-mail required": "Proszę, podaj swój adres e-mail",
      "Not an e-mail address": "Nieprawidłowy adres e-mail",
      "Submit form": "Zatwierdź",
      "Language header": "Język tekstu",
      "You need to choose text language": "Musisz wybrać język tekstu",
      "Parameters": "Parametry",
      "Topics number": "Ilość tematów",
      "Number of iterations": "Liczba iteracji",
      "Top tokens": "Liczba najkepszych modeli"

    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en",

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;
